# Remote Desktop
Remote Desktop  in Java Swing

1. RemoteServer 

This is the server part which waits for clients connections and per each connected client, a new frame appears showing the current client screen. When you move the mouse over the frame, this results in moving the mouse at the client side. The same happens when you right/left click mouse button or type a key while the frame is in focus.

2. RemoteClient  

This the client side, its core function is sending a screen shot of the client's desktop every predefined amount of time. Also it receives server commands such as "move the mouse command", then executes the command at the client's PC.  

#Coding Structure    

RemoteServer
ServerInitiator  Class

This is the entry class which listens to server port and wait for clients connections. Also, it creates an essential part of the program GUI.
ClientHandler Class 

Per each connected client, there is an object of this class. It shows an InternalFrame per client and it receives clients' screen dimension.
ClientScreenReciever Class 

Receives captured screen from the client, then displays it.
ClientCommandsSender Class

It listens to the server commands, then sends them to the client. Server commands include mouse move, key stroke, mouse click, etc.
EnumCommands Class

Defines constants which are used to represent server commands.
RemoteClient   
ClientInitiator Class 

This is the entry class that starts the client instance. It establishes connection to the server and creates the client GUI.
ScreenSpyer Class  

Captures screen periodically and sends them to the server.
ServerDelegate Class   

Receives server commands and executes them in the client PC.
EnumCommands Class  

Defines constants which are used to represent server commands.  
